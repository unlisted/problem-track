#!/bin/bash
set -e

db_name='track'
max_tries=10

echo 'Waiting for Mysql to be available'
tries=${max_tries}
while [ $tries -gt 0 ] && ! mysql -h db -u root --password=555444 -e "SELECT User FROM mysql.user" mysql; do
    echo "Mysql attempts remaining: ${tries}"
    tries=$(($tries-1))
    sleep 5
done
echo
if [ $tries -le 0 ]; then
    echo >&2 "error: unable to contact Mysql after ${max_tries} tries"
    exit 1
fi
if ! mysql -h db -u root --password=555444 $db_name -e "DESCRIBE django_migrations"; then
    echo "Initial run; applying migrations to database ${db_name}"
    python /var/task/manage.py migrate
fi

exec python /var/task/manage.py runserver 0.0.0.0:8000