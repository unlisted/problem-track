FROM lambci/lambda:build-python3.6
WORKDIR /root
ADD requirements.txt /root
RUN yum install -y mysql; yum clean all &&\
    yum install -y mysql-devel; yum clean all
RUN pip install -r requirements.txt
WORKDIR /var/task
