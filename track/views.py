from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView
from django.http import HttpResponse, Http404
from django.db.models import Count, Avg
from django.template import loader
from django import forms
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from .models import Problem, Rating, ClimbingGym

import datetime


class GymSelectionForm(forms.Form):
    gym = forms.ChoiceField(choices=[(x.name, x.name) for x in ClimbingGym.objects.all()])

@login_required
def problem(request, pid=None):

    if request.method == 'POST':
        form = GymSelectionForm(request.POST)
        if form.is_valid():
            probs = Problem.objects.filter(location__climbing_gym__name__contains=form.cleaned_data['gym']).annotate(
                avg_rating=Avg('rating__rating'))
    else:
        form = GymSelectionForm()
        probs = Problem.objects.annotate(avg_rating=Avg('rating__rating'))

    context = {}
    context['setter'] = Group.objects.get(name='setters') in request.user.groups.all()
    if pid is None:
        context['problems'] = probs
        context['form'] = form
        return render(request, 'track/index.html', context)

    problem = get_object_or_404(probs, pk=pid)
    context['problem'] = problem

    return render(request, 'track/problem.html', context)


class ProblemCreate(CreateView):
    model = Problem
    fields = '__all__'
    template_name = 'track/problem_create.html'


def test(request, pid=None):
    context = {}
    return render(request, 'track/test.html', context)


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ['grade', 'comment_text']
    rating = forms.IntegerField(min_value=1, max_value=10)


@login_required
def feedback(request, pid):
    prob = Problem.objects.get(pk=pid)
    if request.method == 'GET':
        form = FeedbackForm()


        # get all the grades
        ratings = Rating.objects.filter(problem_id=pid)
        for rating in ratings:
            print(rating.grade.grade_value)

        context = {'form': form, 'problem': prob}
        return render(request, 'track/leavefeedback.html', context)
    else:
        # save object to db
        form = FeedbackForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            rating = Rating(email=request.user.email,
                            handle=request.user.username,
                            rating=form.cleaned_data['rating'],
                            grade=form.cleaned_data['grade'],
                            comment_text=form.cleaned_data['comment_text'],
                            date_created=datetime.datetime.now(),
                            date_updated=datetime.datetime.now(),
                            visible=True, ip=request.META.get('REMOTE_ADDR', '0.0.0.0'),
                            problem=prob)
            rating.save()

        return redirect(problem, pid)


class FeebackDetailView(DetailView):
    model = Rating
    template_name = 'track/feedback.html'

    def get_context_data(self, **kwargs):
        context = super(FeebackDetailView, self).get_context_data(**kwargs)
        return context


class ProblemUpdateView(UpdateView):
    model = Problem
    fields = '__all__'
    template_name = 'track/problem_update.html'

    def get_success_url(self, *args, **kwargs):
        return reverse('track-problem')
