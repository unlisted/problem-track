# Generated by Django 2.0.6 on 2018-07-31 18:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('track', '0016_auto_20180731_1354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='problem',
            name='image',
            field=models.ImageField(default='images/None/no-img.jpg', upload_to='images/'),
        ),
    ]
