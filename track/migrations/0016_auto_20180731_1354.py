# Generated by Django 2.0.6 on 2018-07-31 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('track', '0015_problem_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='problem',
            name='image',
            field=models.ImageField(upload_to='images'),
        ),
    ]
