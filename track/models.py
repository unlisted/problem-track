from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class ClimbingGym(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    contact = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class InternalLocation(models.Model):
    description = models.CharField(max_length=100)
    climbing_gym = models.ForeignKey(ClimbingGym, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return str(self.climbing_gym) + ':' + self.description


class Grade(models.Model):
    grade_value = models.CharField(max_length=100)

    @property
    def value(self):
        idx = self.grade_value.index('V')
        return int(self.grade_value[idx+1:])

    def __str__(self):
        return self.grade_value


class Problem(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    location = models.ForeignKey(InternalLocation, on_delete=models.SET_NULL, null=True)
    setters = models.ManyToManyField(User)
    set_date = models.DateField()
    active = models.BooleanField(default=False)
    grade = models.ForeignKey(Grade, on_delete=models.SET_NULL, null=True)
    image = models.ImageField(upload_to='images/', default='images/None/no-img.jpg')


    def get_absolute_url(self):
        return reverse('track-problem')

    def __str__(self):
        return self.name


class Rating(models.Model):
    email = models.EmailField(null=True)
    handle = models.CharField(max_length=25)
    ip = models.GenericIPAddressField(null=True)
    problem = models.ForeignKey(Problem, on_delete=models.SET_NULL, null=True)
    date_created = models.DateField(null=True)
    date_updated = models.DateField(null=True)
    rating = models.IntegerField()
    grade = models.ForeignKey(Grade, on_delete=models.SET_NULL, null=True)
    comment_text = models.TextField(max_length=300, null=True)
    visible = models.BooleanField(default=False)

    def __str__(self):
        return str(self.problem) + ':' + str(self.handle)


