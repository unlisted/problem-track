from django.contrib import admin

from .models import Problem, Rating, ClimbingGym, InternalLocation, Grade
# Register your models here.
admin.site.register(InternalLocation)
admin.site.register(ClimbingGym)
admin.site.register(Problem)
admin.site.register(Rating)
admin.site.register(Grade)
