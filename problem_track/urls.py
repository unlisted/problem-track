"""problem_track URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from track import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('problem/', views.problem, name='track-problem'),
    path('problem/add/', views.ProblemCreate.as_view(), name='track-problemcreate'),
    path('problem/<int:pid>', views.problem, name='track-pid-problem'),
    path('problem/<int:pk>/update', views.ProblemUpdateView.as_view(), name='track-pk-problemupdate'),
    path('leavefeedback/<int:pid>', views.feedback, name='track-pid-leavefeedback'),
    path('feedback/<int:pk>', views.FeebackDetailView.as_view(), name='feedback-pk-detail'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('test/', views.test, name='track-test')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
